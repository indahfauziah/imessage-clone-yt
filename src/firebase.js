// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from 'firebase';


// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBai9vfOyCcUpuDsGrlYj05gWCnrL7ARsA",
  authDomain: "imessage-clone-yt-c1418.firebaseapp.com",
  projectId: "imessage-clone-yt-c1418",
  storageBucket: "imessage-clone-yt-c1418.appspot.com",
  messagingSenderId: "904818356571",
  appId: "1:904818356571:web:26b9d324d264dfa475aa7e",
  measurementId: "G-DB39GK8QQF"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

const auth = firebase.auth();

const provider = new firebase.auth.GoogleAuthProvider();

export {auth, provider};

export default db;